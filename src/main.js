import Vue from 'vue';
import App from './App.vue';
import firebase from 'firebase';
import './assets/reset.css';
import chooseTranslations from '@/services/utils.js';

const firebaseConfig = {
  apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
  authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.VUE_APP_FIREBASE_APP_ID,
  measurementId: process.env.VUE_APP_FIREBASE_MEASUREMENT_ID,
};

Vue.config.productionTip = false;
firebase.initializeApp(firebaseConfig);

// Choose the translations depending on the selected language
Vue.filter('lang', chooseTranslations)

// Register global icon components
const requireComponent = require.context(
  './components',
  false,
  /Icon[A-Z]\w+\.(vue)$/,
);

requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName);

  const componentName = fileName
    .replace(/^\.\//, '')
    .replace(/\.\w+$/, '');

  Vue.component(
    componentName,
    componentConfig.default || componentConfig,
  );
});

new Vue({
  render: h => h(App),
}).$mount('#app');
