module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
  ],
  important: true,
  theme: {
    screens: {
      'sm': '640px',
      'hd': '1280px',
      'fhd': '1920px',
      '4k': '3840px',
    },
    fontSize: {
      xs: '0.75rem',
      sm: '0.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '4.5rem',
      '8xl': '6rem',
      '9xl': '7rem',
    },
    extend: {
      colors: {
        summer: {
          pink: '#fad6e1',
          marine: '#86e3ce',
          green: '#d0e6a5',
          yellow: '#ffdd95',
          red: '#fc887b',
          lilac: '#ccabda',
          orange: "#ffb284",
        },
        winter: {
          lilac: '#8a5082',
          cyan: '#aac9ce',
          pink: '#e7c1ce',
          yellow: '#f4dcd0',
          red: '#e89897',
          blue: '#7facd6',
        },
        dark: {
          rose: '#271127',
          marine: '#112727',
          code: '#131417',
          card: '#1d1e22',
          text: '#c5c8d4',
          green: '#0e150d'
        }
      },
      margin: {},
      animation: ['hover'],
      height: {
        '98': '28rem',
        '100': '34rem',
        '110': '44rem',
        '114': '50rem',
        '118': '64rem',
        '120': '74rem',
       },
       width: {
        '90': '22rem',
        '98': '28rem',
        '100': '30rem',
        '110': '44rem',
        '120': '50rem',
       }
    },
  },
  variants: {},
  plugins: [],
  darkMode: 'class',
};
