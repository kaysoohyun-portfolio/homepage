import * as en from './i18n/en.json';
import * as es from './i18n/es.json';

export default function chooseTranslations(key, section) {
  let lang = localStorage.lang === 'es' ? es : en;
  return lang.default[section][key] || key;
}